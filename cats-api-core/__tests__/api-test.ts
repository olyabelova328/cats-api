import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';

const cats: CatMinInfo[] = [{ name: 'Котиксотличнымименем', description: '', gender: 'male' }];



const HttpClient = Client.getInstance();

describe('API  get_by_id', () => {
    let coreCatId;
    let notExistCatId = '12345';
    beforeAll(async () => {
        try {
            const add_cat_response = await HttpClient.post('core/cats/add', {
                responseType: 'json',
                json: { cats },
            });
            if ((add_cat_response.body as CatsList).cats[0].id) {
                coreCatId = (add_cat_response.body as CatsList).cats[0].id;
            } else throw new Error('Не получилось получить id тестового котика!');
        } catch (error) {
            throw new Error('Не удалось создать котика для автотестов!');
        }
    });

  afterAll(async () => {
    await HttpClient.delete(`core/cats/${coreCatId}/remove`, {
      responseType: 'json',
    });
  });

  
    it('Получение существющего id', async () => {
        const response: any = await HttpClient.get(`core/cats/get-by-id/?id=${coreCatId}`, {
            responseType: 'json',
        });
        expect(response.statusCode).toEqual(200);

        expect(response.body.cat).toEqual({
            id: coreCatId,
            ...cats[0],
            tags: null,
            likes: 0,
            dislikes: 0,
        });
    });

    it('Получение несуществующего id', async () => {
        await expect(
            HttpClient.get(`core/cats/get-by-id/?id=${notExistCatId}`, {
                responseType: 'json',
            })
        ).rejects.toThrowError('Response code 404 (Not Found)');

    });
});

describe('API save-descrioption', () => {
    let coreCatId;
    let newCatDescrioption = "Замечательное описание самого прекрасного котика";

    beforeAll(async () => {
        try {
            const add_cat_response = await HttpClient.post('core/cats/add', {
                responseType: 'json',
                json: { cats },
            });
            if ((add_cat_response.body as CatsList).cats[0].id) {
                coreCatId = (add_cat_response.body as CatsList).cats[0].id;
            } else throw new Error('Не получилось получить id тестового котика!');

        } catch (error) {
            throw new Error('Не удалось подготовить данные для автотеста!');
        }
    });

    afterAll(async () => {
        await HttpClient.delete(`core/cats/${coreCatId}/remove`, {
            responseType: 'json'
        });
    });


    it('Добавление описания', async () => {
        const response: any = await HttpClient.post(`core/cats/save-description`, {
            responseType: 'json',
            json: {
                catId: coreCatId,
                catDescription: newCatDescrioption
            }
        });
        expect(response.statusCode).toEqual(200);

        expect(response.body.description).toEqual(
            newCatDescrioption
        );
    });

    it('Получение по id и проверка описания', async () => {
        const response: any = await HttpClient.get(`core/cats/get-by-id/?id=${coreCatId}`, {
            responseType: 'json',
        });
        expect(response.body.cat.description).toEqual(
            newCatDescrioption
        );
    });
});

describe('API allByLetter', () => {
    let coreCatId;
    it('Проверка что коты находятся в своих группах', async () => {
        const response: any = await HttpClient.get(`core/cats/allByLetter`, {
            responseType: 'json'
        });
        expect(response.statusCode).toEqual(200);

        response.body.groups.forEach(function (group) {
            group.cats.forEach(function (cat) {
                expect(cat.name.charAt(0).toLowerCase()).toEqual(group.title.charAt(0).toLowerCase());
            });
        })
    });

    it('Проверка что коты счётчик котов в группе показывает правильно', async () => {
        const response: any = await HttpClient.get(`core/cats/allByLetter`, {
            responseType: 'json'
        });
        expect(response.statusCode).toEqual(200);

        response.body.groups.forEach(function (group) {
            expect(group.count_in_group).toEqual(group.cats.length);
        });
    });

});